/*
 * %kadu copyright begin%
 * Copyright 2017 Rafał Przemysław Malinowski (rafal.przemyslaw.malinowski@gmail.com)
 * %kadu copyright end%
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qfacebook-login-job.h"

#include "qfacebook-http-api.h"
#include "qfacebook-http-reply.h"
#include "qfacebook-http-request.h"
#include "qfacebook-session.h"

QFacebookLoginJob::QFacebookLoginJob(QFacebookHttpApi &httpApi, QString userName, QString password, QObject *parent) :
		QObject{parent}
{
	auto reply = httpApi.auth(std::move(userName), std::move(password));
	connect(reply, &QFacebookHttpReply::finished, this, &QFacebookLoginJob::replyFinished);
}

QFacebookLoginJob::~QFacebookLoginJob()
{
}

void QFacebookLoginJob::replyFinished(const std::experimental::optional<QJsonObject> &result)
{
	deleteLater();

	if (!result)
		emit finished({QFacebookLoginStatus::Error, std::experimental::nullopt});

	if ((*result)["error_code"].toInt() == 401)
		emit finished({QFacebookLoginStatus::ErrorInvalidPassword, std::experimental::nullopt});
	else
		emit finished({QFacebookLoginStatus::OK, QFacebookSession::fromJson(*result)});
}
