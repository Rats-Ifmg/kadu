project (facebook_protocol)

cmake_minimum_required (VERSION 2.8)

find_package (Kadu REQUIRED CONFIG)

kadu_api_directories (plugins/facebook_protocol
	.
)

set (SOURCES
	qfacebook/qfacebook-contact.cpp
	qfacebook/qfacebook-download-contacts-delta-job.cpp
	qfacebook/qfacebook-download-contacts-job.cpp
	qfacebook/qfacebook-http-api.cpp
	qfacebook/qfacebook-http-reply.cpp
	qfacebook/qfacebook-http-request.cpp
	qfacebook/qfacebook-login-job.cpp
	qfacebook/qfacebook-session.cpp
	qfacebook/qfacebook.cpp

	services/facebook-roster-service.cpp

	widgets/facebook-add-account-widget.cpp
	widgets/facebook-edit-account-widget.cpp

	facebook-account-data.cpp
	facebook-module.cpp
	facebook-plugin-modules-factory.cpp
	facebook-plugin-object.cpp
	facebook-protocol.cpp
	facebook-protocol-factory.cpp
)

kadu_plugin (facebook_protocol
	PLUGIN_SOURCES ${SOURCES}
)
